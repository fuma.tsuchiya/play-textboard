# --- !Ups
CREATE TABLE `posts` (
  `id` BIGINT PRIMARY KEY AUTO_INCREMENT,
  `body` VARCHAR(256) NOT NULL,
  `startDate` DATETIME NOT NULL,
  `endDate` DATETIME NOT NULL,
);

# --- !Downs
DROP TABLE `posts`;
