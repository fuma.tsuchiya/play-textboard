import java.time.LocalDate

case class Post(body: String, startDate: LocalDate, endDate: LocalDate)

val a: (String, LocalDate, LocalDate) =
  ("test",
    LocalDate.of(2019,2,23),
    LocalDate.of(2019,9,9)
  )
println(s"${a._3}")
println(s"${a}")

