package controllers

import java.time.LocalDate
import javax.inject.Inject
import play.api.data.Form
import play.api.data._
import play.api.data.Forms._
import play.api.i18n.{I18nSupport, Messages, MessagesApi}
import play.api.mvc.{AbstractController, Action, Controller, ControllerComponents}
import play.api.libs.json.Json
import java.time.LocalDate
import views.html.defaultpages.error
import Post._

case class PostRequest(body: String)

//class TextboardController @Inject()(val messagesApi: MessagesApi) extends AbstructController with I18nSupport {
class TextboardController @Inject()(cc: ControllerComponents) extends AbstractController(cc) with I18nSupport {
  private[this] val form = Form(
    mapping(
      "post" -> text(minLength = 1, maxLength = 30),
    )(PostRequest.apply)(PostRequest.unapply)
  )

  //bodyをtext(文字列)にバインド
  val addForm = Form(
    mapping(
      "body"      -> text,
      "startDate" -> localDate,
      "endDate"   -> localDate
      //    )(Add.apply)(Add.unapply)
    )(Add.apply)(Add.unapply)
  )

  def get = Action { implicit request =>
    Ok(Json.toJson(Response(Meta(200), Some(Json.obj("posts" -> Json.toJson(PostRepository.findAll))))))
  }

  def post = Action { implicit request =>
    form.bindFromRequest.fold(
      error => {
        val errorMessage = Messages(error.errors("post")(0).message)
        BadRequest(Json.toJson(Response(Meta(400, Some(errorMessage)))))
      },
      postRequest => {
        val post = Post(postRequest.body, LocalDate.now, LocalDate.now)
        PostRepository.add(post)
        Ok(Json.toJson(Response(Meta(200))))
      }
    )
  }

  def gets(date: Option[String]) = Action { implicit request =>
    date match {
      case Some(date) => {
        Ok(views.html.index((PostRepository.find(LocalDate.parse(date))), form))

      }
      //その日のタスクを与える
      case None => Ok(views.html.index((PostRepository.find(LocalDate.now)), form))
    }
  }

  // def add() = Action { implicit request =>
  //   val post = addForm.bindFromRequest.get
  //   //    Ok(views.html.index(PostRepository.add(post), formp))
  //   val posts = Post(post.body, post.startDate, post.endDate)
  //   PostRepository.add(posts)
  //   Redirect("/")
  // }

  def add() = Action { implicit request =>
    val post = addForm.bindFromRequest.get
    //    Ok(views.html.index(PostRepository.add(post), formp))
    val posts = Post(post.body, post.startDate, post.endDate)
    PostRepository.add(posts)
    Redirect("/")
  }
}
