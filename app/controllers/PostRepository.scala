package controllers

import java.time.LocalDate
import scalikejdbc._
import scalikejdbc.jsr310._

object PostRepository {
  //DB readOnly= DB.readOnlyの点が省略
  def findAll: Seq[Post] = DB readOnly { implicit session =>
    sql"SELECT id, body, startDate, endDate FROM posts"
      .map { rs =>
        Post(rs.long("id"), rs.string("body"), rs.localDate("startDate"), rs.localDate("endDate"))
      //list: 取得, apply: 実行
      }
      .list()
      .apply()
  }
  //def find(date: LocalDate) = posts.filter(post => post.startDate == date)
  //  def find(date: LocalDate) = DB readOnly { implicit session =>
  def find(date: LocalDate) = DB readOnly { implicit session =>
    sql"SELECT id, body, startDate, endDate FROM posts"
      .map { rs =>
        Post(rs.long("id"), rs.string("body"), rs.localDate("startDate"), rs.localDate("endDate"))
      }
      .list()
      .apply()
  }

  def add(post: Post): Unit = DB localTx { implicit sessin =>
    sql"INSERT INTO posts (body, startDate, endDate) VALUES (${post.body}, ${post.startDate},${post.endDate})"
      .update()
      .apply()
  }

}
