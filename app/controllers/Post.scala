package controllers
import java.time.LocalDate
import play.api.libs.json.{JsValue, Json, Writes}

case class Post(id: Long, body: String, startDate: LocalDate, endDate: LocalDate)
object Post {
  implicit val writes: Writes[Post] = Json.writes[Post]

  def apply(body: String, startDate: LocalDate, endDate: LocalDate): Post = {
    Post(0, body, startDate, endDate)
  }
}

case class Add(body: String, startDate: LocalDate, endDate: LocalDate)

case class Meta(status: Int, errorMessage: Option[String] = None)

object Meta {
  implicit val writes: Writes[Meta] = Json.writes[Meta]
}

case class Response(meta: Meta, data: Option[JsValue] = None)

object Response {
  implicit def writes: Writes[Response] = Json.writes[Response]
}
