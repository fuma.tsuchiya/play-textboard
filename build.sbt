name := """textboard"""
organization := "com.example"
version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)
scalaVersion := "2.12.8"
val scalikejdbcVersion = "3.3.4"
libraryDependencies += guice
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "4.0.1" % Test
libraryDependencies ++= Seq(
  jdbc,
  evolutions,
  guice,
  filters,
  "org.scalatestplus.play" %% "scalatestplus-play"           % "4.0.1" % Test,
  "com.h2database"         % "h2"                            % "1.4.197",
  "org.scalikejdbc"        %% "scalikejdbc"                  % scalikejdbcVersion,
  "org.scalikejdbc"        %% "scalikejdbc-config"           % scalikejdbcVersion,
  "org.scalikejdbc"        %% "scalikejdbc-play-initializer" % "2.7.0-scalikejdbc-3.3",
  "org.scalikejdbc"        %% "scalikejdbc-test"             % scalikejdbcVersion % Test,
  "org.mockito"            % "mockito-core"                  % "2.27.0" % Test
)
// Adds additional packages into Twirl
//TwirlKeys.templateImports += "com.example.controllers._"
// Adds additional packages into conf/routesv
// play.sbt.routes.RoutesKeys.routesImport += "com.example.binders._"
