package controllers

import org.openqa.selenium.WebDriver
import org.openqa.selenium.htmlunit.HtmlUnitDriver
import org.scalatestplus.play._
import org.scalatestplus.play.guice.GuiceOneServerPerTest

class TextboardSpec extends PlaySpec with GuiceOneServerPerTest with OneBrowserPerSuite with HtmlUnitFactory {

  override def createWebDriver(): WebDriver = {
    val driver = new HtmlUnitDriver{
      def setAcceptLanguage(lang: String) =
        this.getWebClient().addRequestHeader("Accept-Language", lang)
    }
    driver.setAcceptLanguage("en")
    driver
  }

  "GET /" should{
    "何も投稿しない場合はメッセージを表示しない" in {
      go to s"http://localhost:$port/"
      assert(pageTitle === "Scala Text Textboard")
      assert(findAll(className("post-body")).length === 0)
    }
  }

  "POST /" should {
    "投稿したものが表示される" in {
      val body = "test post "

      go to s"http://localhost:$port/"
      textField(cssSelector("input#post")).value = body
      submit()

      eventually{
        val posts = findAll(className("post-body")).toSeq
        assert(posts.length === 1)
        assert(posts(0).text === body)
        assert(findAll(cssSelector("p#error")).length === 0)
      }
    }
  }

  "POST /" should{
    "投稿したものが表示される その2" in {
      go to s"http://localhost:$port/"

      eventually{
        val posts = findAll(className("post-body")).toSeq
        assert(posts.length === 1)
      }
    }

    "空のメッセージは投稿できない" in {
      val body = ""

      go to s"http://localhost:$port/"
      textField(cssSelector("input#post")).value = body
      submit()

      eventually{
        val error = findAll(cssSelector("p#error")).toSeq
        assert(error.length === 1)
        assert(error(0).text === "Please enter a message....")
      }
    }

    "長すぎるメッセージは投稿できない" in {
      val body= "too long messages"

      go to s"http://localhost:$port/"
      textField(cssSelector("input#post")).value = body
      submit()

      eventually{
        val error = findAll(cssSelector("p#error")).toSeq
        assert(error.length === 1)
      }
    }
  }
}



